import React from 'react';
import {View, StyleSheet, Text, TouchableOpacity} from 'react-native';
import Colors from './../utils/colors';
const DrawerItem = props => {
  return (
    <TouchableOpacity style={styles.container} onPress={props.onPressAction}>
      <Text style={styles.textStyle}>{props.text}</Text>
    </TouchableOpacity>
  );
};
export default DrawerItem;
const styles = StyleSheet.create({
    container:{
        marginTop:20
    },
    textStyle:{
        fontSize:18,
        lineHeight:20,
        color:Colors.black,
        fontWeight:"600"
    }
});
