import React from 'react';
import {View, StyleSheet, Text, TouchableOpacity} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Colors from './../utils/colors';
import Header from './Header';
import DrawerItem from './DrawerItem';
const DrawerComponent = props => {
  const {navigation} = props;
  return (
    <View style={styles.container}>
      <Header
        leftButtonContainerStyle={{justifyContent: 'flex-end'}}
        leftIconChildren={
          <Text style={styles.headerText}>
            Hello,<Text style={{color: Colors.black}}> Jeep</Text>
          </Text>
        }
        rightIconChildren={
          <AntDesign name={'close'} size={22} color={Colors.black} />
        }
        onRightAction={() => {
          navigation.closeDrawer();
        }}
        onLeftAction={() => {
          navigation.navigate('Home');
        }}
      />
      <View style={styles.contentContainer}>
        {/* <DrawerItem
          onPressAction={() => {
            props.navigation.closeDrawer();
          }}
          text={'Friends Page'}
        /> */}
        <DrawerItem
          onPressAction={() => {
            navigation.navigate('Dashboard');
          }}
          text={'Dashboard'}
        />
        <DrawerItem
          onPressAction={() => {
            navigation.navigate('TouchPoint');
          }}
          text={'Touch Points'}
        />
        <DrawerItem
          onPressAction={() => {
            navigation.navigate('Contact');
          }}
          text={'Contact'}
        />
        <DrawerItem
          onPressAction={() => {
            props.navigation.closeDrawer();
          }}
          text={'Logout'}
        />
      </View>
    </View>
  );
};
export default DrawerComponent;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
    paddingTop: 28,
    paddingLeft: 20,
    paddingRight: 20,
  },
  headerText: {
    fontSize: 16,
    fontWeight: 'bold',
    color: Colors.primaryBlue,
  },
  contentContainer: {
    flex: 1,
    alignItems: 'center',
    marginTop: 20,
  },
});
