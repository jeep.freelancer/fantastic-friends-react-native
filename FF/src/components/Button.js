import React from 'react';
import {View, StyleSheet, Text, TouchableOpacity} from 'react-native';
import colors from './../utils/colors';
const Button = props => {
  return (
    <TouchableOpacity
      onPress={props.clickAction}
      style={[style.mainContainer, props.backgroundColorStyle]}>
      <Text style={[style.txt, props.textStyle]}>{props.text}</Text>
    </TouchableOpacity>
  );
};
export default Button;
const style = StyleSheet.create({
  mainContainer: {
    alignItems: 'center',
    height: 60,
    overflow: 'hidden',
    justifyContent: 'center',
    borderRadius: 30,
    flexDirection: 'row',
    backgroundColor: colors.primaryBlue,
  },
  innerContainer: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height: 35,
    borderRadius: 14,
  },
  txt: {
    color: colors.textSplash,
    fontSize: 16,
    fontWeight: 'bold',
  },
});
