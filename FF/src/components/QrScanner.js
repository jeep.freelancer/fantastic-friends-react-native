import React from 'react';
import {View, StyleSheet, Text, TouchableOpacity, Linking} from 'react-native';
import colors from './../utils/colors';
import QRCodeScanner from 'react-native-qrcode-scanner';
const QrScanner = props => {
  return (
    <View style={styles.container}>
      <QRCodeScanner
        onRead={props.onSuccess}
        // flashMode={RNCamera.Constants.FlashMode.torch}
        topContent={
          <Text style={styles.descText}>Move your camera toward QR code!</Text>
        }
      />
    </View>
  );
};
export default QrScanner;
const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    backgroundColor: colors.white,
    top: 0,
    bottom: 0,
    right: 0,
    left: 0,
  },
  descText: {
    fontSize: 18,
    fontWeight: 'bold',
    color: colors.primaryBlue,
  },
});
