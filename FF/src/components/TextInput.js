import React, {useState} from 'react';
import {View, StyleSheet, TextInput, Text} from 'react-native';
import colors from './../utils/colors';
import Ionicons from 'react-native-vector-icons/Ionicons';
const Input = props => {
  const [focused, setFocused] = useState(false);
  return (
    <View style={[styles.mainContainer, props.containerStyle]}>
      <View style={styles.textInputContainer}>
        {!!focused && (
          <Text style={styles.floatingText}>{props.placeholder}</Text>
        )}
        <TextInput
          secureTextEntry={props.secure}
          style={styles.inputStyle}
          placeholder={props.placeholder}
          placeholderTextColor={colors.primaryGray}
          onChangeText={props.onChangeText}
          onFocus={() => {
            setFocused(true);
          }}
          onBlur={() => {
            setFocused(false);
          }}
          value={props.value}
        />
      </View>

      <View style={styles.checkContainer}>
        {!!props.isChecked && (
          <Ionicons
            name={'checkmark-sharp'}
            color={colors.primaryGreen}
            size={22}
          />
        )}
      </View>
    </View>
  );
};
export default Input;
const styles = StyleSheet.create({
  mainContainer: {
    alignItems: 'center',
    height: 64,
    overflow: 'hidden',
    justifyContent: 'center',
    borderRadius: 10,
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: colors.primaryGray,
    padding: 10,
  },
  textInputContainer: {
    width: '92%',
  },
  checkContainer: {
    width: '8%',
  },
  floatingText: {
    position: 'absolute',
    color: colors.primaryGray,
    fontSize: 12,
    top: -5,
    left: 4,
  },
  inputStyle:{
    color:colors.primaryGray
  }
});
