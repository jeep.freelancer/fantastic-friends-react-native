import React from 'react';
import {View, StyleSheet, Text, TouchableOpacity, Image} from 'react-native';
import colors from './../utils/colors';
const LakeBox = props => {
  return (
    <TouchableOpacity
      onPress={props.clickAction}
      style={[
        style.mainContainer,
        props.backgroundColorStyle,
        {borderColor: props.selected ? colors.primaryBlue : colors.primaryGray},
      ]}>
      <Image style={style.pic} source={props.pic} />
      <Text style={[style.txt, props.textStyle]}>{props.text}</Text>
    </TouchableOpacity>
  );
};
export default LakeBox;
const style = StyleSheet.create({
  mainContainer: {
    borderWidth: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-between',
    width: '46%',
    height: 164,
    margin: 8,
    padding: 10,
    borderRadius: 10,
  },
  pic: {
    width: 80,
    height: 100,
    resizeMode: 'contain',
  },
  txt: {
    color: colors.black,
    fontSize: 18,
    fontWeight: 'bold',
  },
});
