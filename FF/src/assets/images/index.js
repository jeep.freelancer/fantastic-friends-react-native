const images = {
  splashLogo: require('./splashLogo.png'),
  onBoarding: require('./onBoarding.png'),
  Conesus: require('./Conesus.png'),
  Hemlock: require('./Hemlock.png'),
  Canadice: require('./Canadice.png'),
  Cayuga: require('./Cayuga.png'),
  Honeoye: require('./Honeoye.png'),
  Keuka: require('./Keuka.png'),
  Otisco: require('./Otisco.png'),
  Owasco: require('./Owasco.png'),
  Seneca: require('./Seneca.png'),
  Skaneateles: require('./Skaneateles.png'),
  Canandaigua: require('./Canandaigua.png'),
  QrCode: require('./QrCode.png'),
  UsersRise: require('./UsersRise.png'),
};
export default images;
