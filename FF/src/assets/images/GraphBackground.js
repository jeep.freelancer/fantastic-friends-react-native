import * as React from 'react';
import Svg, {Path,Rect,Line,G,Text,TSpan} from 'react-native-svg';

function GraphBackground(props) {
  return (
    
<Svg xmlns="http://www.w3.org/2000/svg" width="201.5" height="327" viewBox="0 0 201.5 327">
  <G id="Group_9" data-name="Group 9" transform="translate(299 219.5)">
    <Rect id="Rectangle_27" data-name="Rectangle 27" width="201" height="327" rx="10" transform="translate(-299 -219.5)" fill="#bbdaec"/>
    <Line id="Line_1" data-name="Line 1" x2="201" transform="translate(-298.5 -182)" fill="none" stroke="#fff" stroke-width="2" stroke-dasharray="2 2"/>
    <Line id="Line_2" data-name="Line 2" x2="201" transform="translate(-298.5 -125)" fill="none" stroke="#fff" stroke-width="2" stroke-dasharray="2 2"/>
    <Line id="Line_3" data-name="Line 3" x2="201" transform="translate(-298.5 -65)" fill="none" stroke="#fff" stroke-width="2" stroke-dasharray="2 2"/>
    <Line id="Line_4" data-name="Line 4" x2="201" transform="translate(-298.5 -3)" fill="none" stroke="#fff" stroke-width="2" stroke-dasharray="2 2"/>
    <Line id="Line_5" data-name="Line 5" x2="201" transform="translate(-298.5 54)" fill="none" stroke="#fff" stroke-width="2" stroke-dasharray="2 2"/>
    <Text id="_100_" data-name="100%" transform="translate(-291 -189)" fill="#fff" font-size="12" font-family="SegoeUI, Segoe UI"><TSpan x="0" y="0">100%</TSpan></Text>
    <Text id="_75_" data-name="75%" transform="translate(-291 -132)" fill="#fff" font-size="12" font-family="SegoeUI, Segoe UI"><TSpan x="0" y="0">75%</TSpan></Text>
    <Text id="_50_" data-name="50%" transform="translate(-291 -72)" fill="#fff" font-size="12" font-family="SegoeUI, Segoe UI"><TSpan x="0" y="0">50%</TSpan></Text>
    <Text id="_25_" data-name="25%" transform="translate(-291 -10)" fill="#fff" font-size="12" font-family="SegoeUI, Segoe UI"><TSpan x="0" y="0">25%</TSpan></Text>
    <Text id="_0_" data-name="0%" transform="translate(-291 47)" fill="#fff" font-size="12" font-family="SegoeUI, Segoe UI"><TSpan x="0" y="0">0%</TSpan></Text>
  </G>
</Svg>

  );
}

export default GraphBackground;
