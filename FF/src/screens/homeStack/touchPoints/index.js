import React, {useEffect} from 'react';
import {
  View,
  Image,
  Text,
  StyleSheet,
  TouchableOpacity,
  Linking,
} from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
//Utils
import Colors from '../../../utils/colors';
//Assets
import Images from '../../../assets/images';
//Components
import Button from '../../../components/Button';
import Header from '../../../components/Header';

const TouchPoint = ({navigation}) => {
  const Navigate = stackName => {
    navigation.navigate(stackName);
  };
  const openUrl = url => {
    Linking.openURL(url);
  };
  return (
    <View style={styles.container}>
      <Header
        leftButtonContainerStyle={{justifyContent: 'flex-end'}}
        leftIconChildren={
          <Text style={styles.headerText}>
            Hello,<Text style={{color: Colors.black}}> Jeep</Text>
          </Text>
        }
        rightIconChildren={
          <Feather name={'menu'} size={22} color={Colors.black} />
        }
        onRightAction={() => {
          navigation.openDrawer();
        }}
        onLeftAction={() => {
          navigation.navigate('Home');
        }}
      />
      <Text style={styles.titleText}>{`Touch Points`}</Text>
      <Text style={styles.textDesc}>
        {'Add To Your Finger Lakes Museum Experience'}
      </Text>
      <View style={[styles.buttonsView, {marginTop: 32}]}>
        <TouchableOpacity
          onPress={() => navigation.navigate('CompleteATrail')}
          style={[styles.alphaButton, {backgroundColor: '#2C15BC'}]}>
          <Text style={styles.buttonText}>{'Complete a trail'}</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() =>
            openUrl(
              'https://flmfriendsdonate.securepayments.cardpointe.com/pay',
            )
          }
          style={[styles.alphaButton, {backgroundColor: '#5AA56B'}]}>
          <Text style={styles.buttonText}>{'Donate'}</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.buttonsView}>
        <TouchableOpacity
          onPress={() => navigation.navigate('Home')}
          style={[styles.alphaButton, {backgroundColor: '#34BA59'}]}>
          <Text style={styles.buttonText}>{'Add a Friend'}</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => navigation.navigate('Dashboard')}
          style={[styles.alphaButton, {backgroundColor: '#9440A2'}]}>
          <Text style={styles.buttonText}>{'My Dashboard'}</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.buttonsView}>
        <TouchableOpacity
          onPress={() => navigation.navigate('Volunteer')}
          style={[styles.alphaButton, {backgroundColor: '#E88239'}]}>
          <Text style={styles.buttonText}>{'Volunteer'}</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => navigation.navigate('AttendEvent')}
          style={[styles.alphaButton, {backgroundColor: '#D12626'}]}>
          <Text style={styles.buttonText}>{'Attend an Event'}</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};
export default TouchPoint;
export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.transparent,
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 28,
  },
  titleText: {
    color: Colors.primaryBlue,
    fontWeight: 'bold',
    fontSize: 34,
    marginTop: 16,
  },
  headerText: {
    fontSize: 16,
    fontWeight: 'bold',
    color: Colors.primaryBlue,
  },
  textDesc: {
    fontSize: 14,
    lineHeight: 20,
    fontWeight: '400',
    marginTop: 24,
    letterSpacing: 1,
  },
  alphaButton: {
    height: 80,
    width: '48%',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonsView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 16,
  },
  buttonText: {
    color: Colors.white,
    fontSize: 17,
    lineHeight: 20,
    fontWeight: 'bold',
  },
});
