import React, {useEffect} from 'react';
import {
  View,
  Image,
  Text,
  StyleSheet,
  Linking,
  TouchableOpacity,
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
//Utils
import Colors from '../../../utils/colors';
//Assets
import Images from '../../../assets/images';
//Components
import Button from '../../../components/Button';
import Header from '../../../components/Header';

const TrailProgress = ({navigation}) => {
    const openUrl = url => {
        Linking.openURL(url)
    };
  return (
    <View style={styles.container}>
      <Header
        leftIconChildren={
          <Ionicons name={'chevron-back'} size={22} color={Colors.black} />
        }
        onLeftAction={() => {
          navigation.goBack();
        }}
      />
      <Text style={styles.titleText}>{`Trail Progress`}</Text>
      <Text style={styles.desctext}>
      You have completed 0 out of 60 trails (0%).
      </Text>

      <View style={styles.progressBar}/>

      <TouchableOpacity
        onPress={() => {
          openUrl('https://www.gofingerlakes.org/map');
        }}
        style={styles.linkTextPressable}>
        <Text style={styles.linkText}>Go Finger Lakes - Trails List</Text>
      </TouchableOpacity>
    </View>
  );
};
export default TrailProgress;
export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.transparent,
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 28,
  },
  titleText: {
    color: Colors.primaryBlue,
    fontWeight: 'bold',
    fontSize: 34,
    marginTop: 16,
  },
  friendsButton: {
    marginTop: 40,
    width: '90%',
    alignSelf: 'center',
    backgroundColor: Colors.PrimaryBrown,
  },
  desctext: {
    fontWeight: '400',
    fontSize: 14,
    lineHeight: 20,
    letterSpacing: 1,
    marginTop: 24,
  },
  linkText: {
    color: Colors.primaryBlue,
    textDecorationLine: 'underline',
    fontSize: 14,
    lineHeight: 25,
    fontWeight: '600',
    textAlign: 'center',
  },
  linkTextPressable: {
    marginTop: 20,
  },
  progressBar:{
    width:"100%",
    height:30,
    borderRadius:100,
    borderWidth:1,
    borderColor:Colors.primaryGray,
    marginTop:34,
    marginBottom:20

  }
});
