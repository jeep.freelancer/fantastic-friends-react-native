import React, {useEffect,useState} from 'react';
import {View, Image, Text, StyleSheet, Linking} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
//Utils
import Colors from '../../../utils/colors';
//Assets
import Images from '../../../assets/images';
//Components
import Button from '../../../components/Button';
import Header from '../../../components/Header';
import QrScanner from '../../../components/QrScanner';

const CompleteATrail = ({navigation}) => {
  const [scanQr, setScanQr] = useState(false);
  return (
    <View style={styles.container}>
      <Header
        leftIconChildren={
          <Ionicons name={'chevron-back'} size={22} color={Colors.black} />
        }
        onLeftAction={() => {
          navigation.goBack();
        }}
      />
      <Text style={styles.titleText}>{`Complete a trail`}</Text>
      <Text style={styles.desctext}>
        The Finger Lakes Museum is pleased to collaborate with the Finger Lakes
        Land Trust. See a full trails list on their Go Finger Lakes website. At
        each trail head, you will find a QR code that you can scan to get a
        trail “touch point”. Use the app to track how many you have done on your
        “Trails Dashboard”.
      </Text>

      <Button
        clickAction={() => {
          setScanQr(true);
        }}
        backgroundColorStyle={styles.friendsButton}
        textStyle={{color: Colors.black,fontWeight:"bold"}}
        text={'SCAN QR CODE'}
      />
      {!!scanQr && (
        <QrScanner
          onSuccess={e => {
            setScanQr(false);
            openUrl(e.data);
          }}
        />
      )}
    </View>
  );
};
export default CompleteATrail;
export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.transparent,
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 28,
  },
  titleText: {
    color: Colors.primaryBlue,
    fontWeight: 'bold',
    fontSize: 34,
    marginTop: 16,
  },
  friendsButton: {
    marginTop: 40,
    width: '90%',
    alignSelf: 'center',
    backgroundColor: Colors.PrimaryBrown,
  },
  desctext:{
      fontWeight:"400",
      fontSize:14,
      lineHeight:20,
      letterSpacing:1,
      marginTop:24
  }
});
