import React, {useEffect, useState} from 'react';
import {
  View,
  Image,
  Text,
  StyleSheet,
  Linking,
  TouchableOpacity,
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Carousel, {Pagination} from 'react-native-snap-carousel';
//Utils
import Colors from '../../../utils/colors';
import {width, height} from '../../../utils/Dimensions';
//Assets
import Images from '../../../assets/images';
import GraphBackground from '../../../assets/images/GraphBackground';
import GraphLake from '../../../assets/images/GraphLake';
// import KeukaLake from '../../../assets/images/keukaLake.svg'
//Components
import Button from '../../../components/Button';
import Header from '../../../components/Header';

const data = [
  {id: 1, name: 'Conesus'},
  {id: 2, name: 'Conesus'},
  {id: 3, name: 'Conesus'},
  {id: 4, name: 'Conesus'},
  {id: 5, name: 'Conesus'},
  {id: 6, name: 'Conesus'},
];

const LakeParticipation = ({navigation}) => {
  const [activeSlide, setActiveSlide] = useState(0);
  const _renderItem = ({item, index}) => {
    return (
      <View style={{alignItems: 'center'}}>
        <View style={{alignItems: 'center'}}>
          <GraphBackground />
          <View style={styles.lakeView}>
            <GraphLake />
          </View>
          <Text style={styles.graphTitle}>{item.name}</Text>
        </View>
        <Text style={styles.itemDesc}>
          Swipe to see how other lake participation compares
        </Text>
      </View>
    );
  };
  const openUrl = url => {
    Linking.openURL(url);
  };
  return (
    <View style={styles.container}>
      <Header
        leftIconChildren={
          <Ionicons name={'chevron-back'} size={22} color={Colors.black} />
        }
        onLeftAction={() => {
          navigation.goBack();
        }}
        containerStyle={{paddingLeft: 20}}
      />
      <Text style={styles.titleText}>{`Lake Participation`}</Text>
      <View style={{paddingTop: 60}}>
        <Carousel
          // firstItem={1}
          data={data}
          renderItem={_renderItem}
          sliderWidth={width}
          itemWidth={width}
          onSnapToItem={index => {
            setActiveSlide(index);
          }}
        />
        <Pagination
          dotsLength={data.length}
          activeDotIndex={activeSlide}
          dotStyle={{
            width: 10,
            height: 10,
            borderRadius: 5,
            marginHorizontal: 0,
            backgroundColor: Colors.primaryBlue,
          }}
          inactiveDotOpacity={0.4}
          inactiveDotScale={0.6}
        />
        {/* <GraphBackground/> */}
        {/* <KeukaLake/> */}
      </View>
    </View>
  );
};
export default LakeParticipation;
export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.transparent,
    // paddingLeft: 20,
    // paddingRight: 20,
    paddingTop: 28,
  },
  titleText: {
    color: Colors.primaryBlue,
    fontWeight: 'bold',
    fontSize: 34,
    marginTop: 16,
    paddingLeft: 20,
  },
  friendsButton: {
    marginTop: 40,
    width: '90%',
    alignSelf: 'center',
    backgroundColor: Colors.PrimaryBrown,
  },
  desctext: {
    fontWeight: '400',
    fontSize: 14,
    lineHeight: 20,
    letterSpacing: 1,
    marginTop: 24,
  },
  linkText: {
    color: Colors.primaryBlue,
    textDecorationLine: 'underline',
    fontSize: 14,
    lineHeight: 25,
    fontWeight: '600',
    textAlign: 'center',
  },
  linkTextPressable: {
    marginTop: 20,
  },
  progressBar: {
    width: '100%',
    height: 30,
    borderRadius: 100,
    borderWidth: 1,
    borderColor: Colors.primaryGray,
    marginTop: 34,
    marginBottom: 20,
  },
  graphTitle: {
    position: 'absolute',
    bottom: 15,
    fontSize: 22,
  },
  lakeView: {
    position: 'absolute',
    marginTop:22
  },
  itemDesc: {
    marginTop: 40,
    fontSize: 18,
    textAlign: 'center',
    width: '55%',
    color: Colors.primaryGray,
  },
});
