import React, {useEffect} from 'react';
import {View, Image, Text, StyleSheet,Linking} from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
//Utils
import Colors from '../../../utils/colors';
//Assets
import Images from '../../../assets/images';
//Components
import Button from '../../../components/Button';
import Header from '../../../components/Header';

const Contact = ({navigation}) => {
  const openUrl = url => {
      Linking.openURL(url)
  };
  return (
    <View style={styles.container}>
      <Header
        leftButtonContainerStyle={{justifyContent: 'flex-end'}}
        leftIconChildren={
          <Text style={styles.headerText}>
            Hellow,<Text style={{color: Colors.black}}>Jeep</Text>
          </Text>
        }
        rightIconChildren={
          <Feather name={'menu'} size={22} color={Colors.black} />
        }
        onRightAction={() => {
          navigation.openDrawer();
        }}
        onLeftAction={() => {
          navigation.navigate('Home');
        }}
      />
      <Text style={styles.titleText}>{`Contact`}</Text>

      <Button
        clickAction={() => openUrl('https://www.fingerlakesmuseum.org/')}
        backgroundColorStyle={styles.newFriendButton}
        textStyle={{color: Colors.white}}
        text={'Visit the Finger Lakes Website'}
      />
      <Button
        clickAction={() => openUrl('https://btblock.atlassian.net/servicedesk/customer/portal/1')}
        backgroundColorStyle={styles.friendsButton}
        textStyle={{color: Colors.black}}
        text={'See a Problem? Report it to us!'}
      />
    </View>
  );
};
export default Contact;
export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.transparent,
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 28,
  },
  titleText: {
    color: Colors.primaryBlue,
    fontWeight: 'bold',
    fontSize: 34,
    marginTop: 16,
  },
  friendsButton: {
    marginTop: 16,
    width: '90%',
    alignSelf: 'center',
    backgroundColor: Colors.PrimaryBrown,
  },
  newFriendButton: {
    marginTop: 56,
    width: '90%',
    alignSelf: 'center',
  },
  headerText: {
    fontSize: 16,
    fontWeight: 'bold',
    color: Colors.primaryBlue,
  },
});
