import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {createDrawerNavigator} from '@react-navigation/drawer';

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

//Screens
import Dashboard from './dashboard';
import Home from './home';
import TouchPoint from './touchPoints';
import Contact from './contact';
import CompleteATrail from './completeATrail';
import AttendEvent from './attendEvent';
import Volunteer from './volunteer';
import TrailProgress from './trailProgress';
import LakeParticipation from './lakeParticipation';
//Components
import DrawerComponent from '../../components/DrawerComponent';

/** Home Drawer */
const HomeDrawerStack = props => (
  <Drawer.Navigator
    drawerStyle={{backgroundColor: '#009AFF', width: '100%'}}
    headerMode="none"
    initialRouteName="Home"
    drawerContent={props => <DrawerComponent {...props} />}>
    <Drawer.Screen name="Home" component={Home} />
    <Drawer.Screen name="Dashboard" component={Dashboard} />
    <Drawer.Screen name="TouchPoint" component={TouchPoint} />
    <Drawer.Screen name="Contact" component={Contact} />
    <Drawer.Screen name="CompleteATrail" component={CompleteATrail} />
    <Drawer.Screen name="AttendEvent" component={AttendEvent} />
    <Drawer.Screen name="Volunteer" component={Volunteer} />
    <Drawer.Screen name="TrailProgress" component={TrailProgress} />
    <Drawer.Screen name="LakeParticipation" component={LakeParticipation} />
  </Drawer.Navigator>
);

/** Home Stack */
export default HomeStack = () => (
  <Stack.Navigator headerMode="none" initialRouteName="HomeDrawerStack">
    <Stack.Screen name="HomeDrawerStack" component={HomeDrawerStack} />
  </Stack.Navigator>
);
