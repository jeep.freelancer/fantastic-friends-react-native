import React, {useEffect, useState} from 'react';
import {
  View,
  Image,
  Text,
  StyleSheet,
  Linking,
  TouchableOpacity,
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
//Utils
import Colors from '../../../utils/colors';
//Assets
import Images from '../../../assets/images';
//Components
import Button from '../../../components/Button';
import Header from '../../../components/Header';
import QrScanner from '../../../components/QrScanner';

const Volunteer = ({navigation}) => {
  const [scanQr, setScanQr] = useState(false);
  const openUrl = url => {
    Linking.openURL(url).catch(err => console.error('An error occured', err));
  };
  return (
    <View style={styles.container}>
      <Header
        leftIconChildren={
          <Ionicons name={'chevron-back'} size={22} color={Colors.black} />
        }
        onLeftAction={() => {
          navigation.goBack();
        }}
      />
      <Text style={styles.titleText}>{`Volunteer`}</Text>
      <Text style={styles.desctext}>
        Volunteering? Get the event's QR code from the event coordinator and use
        the Scan button below to scan the code and record a touchpoint!
      </Text>

      <Button
        clickAction={() => {
          setScanQr(true);
        }}
        backgroundColorStyle={styles.friendsButton}
        textStyle={{color: Colors.black, fontWeight: 'bold'}}
        text={'SCAN QR CODE'}
      />
      <TouchableOpacity
        onPress={() => {
          openUrl(
            'https://www.fingerlakesmuseum.org/events/open-house-volunteer-site-clean-up/',
          );
        }}
        style={styles.linkTextPressable}>
        <Text style={styles.linkText}>Link to Volunteer Events</Text>
      </TouchableOpacity>
      {!!scanQr && (
        <QrScanner
          onSuccess={e => {
            setScanQr(false);
            openUrl(e.data);
          }}
        />
      )}
    </View>
  );
};
export default Volunteer;
export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.transparent,
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 28,
  },
  titleText: {
    color: Colors.primaryBlue,
    fontWeight: 'bold',
    fontSize: 34,
    marginTop: 16,
  },
  friendsButton: {
    marginTop: 40,
    width: '90%',
    alignSelf: 'center',
    backgroundColor: Colors.PrimaryBrown,
  },
  desctext: {
    fontWeight: '400',
    fontSize: 14,
    lineHeight: 20,
    letterSpacing: 1,
    marginTop: 24,
  },
  linkText: {
    color: Colors.primaryBlue,
    textDecorationLine: 'underline',
    fontSize: 14,
    lineHeight: 25,
    fontWeight: '600',
    textAlign: 'center',
  },
  linkTextPressable: {
    marginTop: 20,
  },
});
