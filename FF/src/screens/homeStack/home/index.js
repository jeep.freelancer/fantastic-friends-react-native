import React, {useEffect} from 'react';
import {
  View,
  Image,
  Text,
  StyleSheet,
  TouchableOpacity,
  Linking,
} from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
//Utils
import Colors from '../../../utils/colors';
//Assets
import Images from '../../../assets/images';
//Components
import Button from '../../../components/Button';
import Header from '../../../components/Header';

const Home = ({navigation}) => {
  const Navigate = stackName => {
    navigation.navigate(stackName);
  };
  return (
    <View style={styles.container}>
      <Header
        leftButtonContainerStyle={{justifyContent: 'flex-end'}}
        leftIconChildren={
          <Text style={styles.headerText}>
            Hello,<Text style={{color: Colors.black}}> Jeep</Text>
          </Text>
        }
        rightIconChildren={
          <Feather name={'menu'} size={22} color={Colors.black} />
        }
        onRightAction={() => {
          navigation.openDrawer();
        }}
      />
      <Text style={styles.titleDesc}>
        {`Have your friend scan your ID to be added to the app and to your friend count. Go to your dashboard to see your impact! Be sure to bookmark this page. Tap the Eagle any time to return to this page.`}
      </Text>
      <View style={styles.contentContainer}>
        <Image style={styles.riseImage} source={Images.UsersRise} />
        <Text style={styles.addFriendDesc}>
          {'Add a friend by having them scan your QR code'}
        </Text>
        <Image style={styles.qrImage} source={Images.QrCode} />
        <Text style={[styles.addFriendDesc, {width: '100%', marginTop: 36}]}>
          {'Or by sending them the link below'}
        </Text>
        <TouchableOpacity
          style={styles.linkPressable}
          onPress={() => {
            Linking.openURL(
              'https://fingerlakes.fantastic-friends.org/home/57',
            );
          }}>
          <Text
            style={
              styles.linkText
            }>{`https://fingerlakes.fantastic-friends.org/home/57`}</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};
export default Home;
export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.transparent,
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 28,
  },
  headerText: {
    fontSize: 16,
    fontWeight: 'bold',
    color: Colors.primaryBlue,
  },
  titleDesc: {
    marginTop: 10,
    fontSize: 14,
    lineHeight: 20,
    fontWeight: '400',
    color: Colors.black,
    letterSpacing: 1,
  },
  contentContainer: {
    flex: 1,
    alignItems: 'center',
  },
  riseImage: {
    width: '45%',
    height: 60,
    resizeMode: 'contain',
    marginTop: 43,
  },
  addFriendDesc: {
    fontSize: 18,
    lineHeight: 24,
    letterSpacing: 1,
    width: '70%',
    textAlign: 'center',
    fontWeight: 'bold',
    marginTop: 21,
  },
  qrImage: {
    width: '55%',
    height: 112,
    resizeMode: 'contain',
    marginTop: 11,
  },
  linkText: {
    color: Colors.primaryBlue,
    textDecorationLine: 'underline',
    fontSize: 14,
    lineHeight: 25,
    fontWeight: '600',
  },
  linkPressable: {
    marginTop: 10,
  },
});
