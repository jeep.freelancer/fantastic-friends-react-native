import React, {useEffect} from 'react';
import {View, Image, Text, StyleSheet} from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
//Utils
import Colors from '../../../utils/colors';
//Assets
import Images from '../../../assets/images';
//Components
import Button from '../../../components/Button';
import Header from '../../../components/Header';

const Dashboard = ({navigation}) => {
  const Navigate = stackName => {
    navigation.navigate(stackName);
  };
  return (
    <View style={styles.container}>
      <Header
        leftButtonContainerStyle={{justifyContent: 'flex-end'}}
        leftIconChildren={
          <Text style={styles.headerText}>
            Hello,<Text style={{color: Colors.black}}> Jeep</Text>
          </Text>
        }
        rightIconChildren={
          <Feather name={'menu'} size={22} color={Colors.black} />
        }
        onRightAction={() => {
          navigation.openDrawer();
        }}
        onLeftAction={() => {
          navigation.navigate('Home');
        }}
      />
      <Text style={styles.titleText}>{`My Dashboard`}</Text>

      <View style={styles.poolView}>
        <View>
          <View style={styles.friendsPool}></View>
          <View style={styles.line} />
          <Text style={styles.poolTitle}>{'Friends'}</Text>
          <Text style={styles.poolvalue}>{'100'}</Text>
        </View>
        <View>
          <View style={styles.activitesPool}></View>
          <View style={styles.line} />
          <Text style={styles.poolTitle}>{'Activities'}</Text>
          <Text style={styles.poolvalue}>{` 59`}</Text>
        </View>
      </View>

      <Button
        clickAction={() => Navigate('LakeParticipation')}
        backgroundColorStyle={styles.newFriendButton}
        textStyle={{color: Colors.white}}
        text={'SEE HOW YOUR LAKE COMPARES'}
      />
      <Button
        clickAction={() => Navigate('TrailProgress')}
        backgroundColorStyle={styles.friendsButton}
        textStyle={{color: Colors.black}}
        text={'VIEW YOUR TRIAL PROGRESS'}
      />
    </View>
  );
};
export default Dashboard;
export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.transparent,
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 28,
  },
  titleText: {
    color: Colors.primaryBlue,
    fontWeight: 'bold',
    fontSize: 34,
    marginTop: 16,
  },
  descText: {
    marginTop: 24,
    lineHeight: 20,
    fontSize: 14,
    color: Colors.black,
    letterSpacing: 2,
    marginBottom: 30,
  },
  lakeImage: {
    width: '100%',
    height: 224,
    resizeMode: 'contain',
  },
  friendsButton: {
    marginTop: 16,
    width: '90%',
    alignSelf: 'center',
    backgroundColor: Colors.PrimaryBrown,
  },
  newFriendButton: {
    marginTop: 47,
    width: '90%',
    alignSelf: 'center',
  },
  bottomDesc: {
    fontSize: 14,
    color: Colors.black,
    marginTop: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    letterSpacing: 1,
    lineHeight: 25,
  },
  headerText: {
    fontSize: 16,
    fontWeight: 'bold',
    color: Colors.primaryBlue,
  },
  poolView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingRight: 20,
    paddingLeft: 20,
    marginTop: 70,
    alignItems: 'flex-end',
  },
  friendsPool: {
    width: 59,
    height: 33,
    backgroundColor: '#A52DBF',
    marginLeft: 10,
  },
  activitesPool: {
    width: 59,
    height: 47,
    backgroundColor: '#2C15BC',
    marginLeft: 10,
  },
  line: {
    width: 79,
    height: 1,
    backgroundColor: Colors.primaryGray,
  },
  poolTitle: {
    fontSize: 18,
    fontWeight: '600',
    lineHeight: 20,
    textAlign: 'center',
    color: Colors.black,
    marginTop: 16,
  },
  poolvalue: {
    fontSize: 64,
    color: Colors.black,
  },
});
