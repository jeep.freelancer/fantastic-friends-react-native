import React, {useEffect} from 'react';
import {View, Image, Text, StyleSheet} from 'react-native';
//assets
import Images from '../../assets/images';
//Utils
import Colors from '../../utils/colors';

const Splash = ({navigation}) => {
  useEffect(() => {
    const timer = setTimeout(() => {
      navigation.navigate('AuthStack');
    }, 3000);
    return () => clearTimeout(timer);
  }, []);
  return (
    <View style={styles.container}>
      <Image style={styles.logoStyle} source={Images.splashLogo} />
    </View>
  );
};
export default Splash;
export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.transparent,
    justifyContent: 'center',
    alignItems: 'center',
  },
  logoStyle: {
    width: '70%',
    height: 190,
    resizeMode: 'contain',
  },
});
