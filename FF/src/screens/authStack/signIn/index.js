import React, {useEffect, useState} from 'react';
import {View, Image, Text, StyleSheet, TouchableOpacity} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
//assets
//Utils
import Colors from '../../../utils/colors';
//Components
import Header from '../../../components/Header';
import Input from '../../../components/TextInput';
import Button from '../../../components/Button';

const SignIn = ({navigation}) => {
  const [email, setEmail] = useState('');
  const [emailCorrect, setEmailCorrect] = useState(false);

  const checkEmail = () => {
    if (
      /^\w+([\.+-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,})+$/.test(email.trim()) == true
    ) {
      setEmailCorrect(true);
    } else {
      setEmailCorrect(false);
    }
  };
  return (
    <View style={styles.container}>
      <Header
        leftIconChildren={
          <Ionicons name={'chevron-back'} size={22} color={Colors.black} />
        }
        onLeftAction={() => {
          navigation.goBack();
        }}
      />
      <Text style={styles.titleText}>Login</Text>
      <Input
        placeholder={'Email'}
        containerStyle={{marginTop: 56}}
        isChecked={emailCorrect}
        onChangeText={text => {
          setEmail(text);
          checkEmail();
        }}
        value={email}
      />
      <Input placeholder={'Password'} containerStyle={{marginTop: 8}} secure={true}/>
      <TouchableOpacity style={styles.forgotPressable}>
        <Text style={styles.forgotText}>{'Forgot Password?'}</Text>
      </TouchableOpacity>
      <Button
        clickAction={() => navigation.navigate('HomeStack')}
        backgroundColorStyle={styles.friendsButton}
        textStyle={{color: Colors.black}}
        text={'LOGIN'}
      />
      <View style={styles.bottomContainer}>
        <TouchableOpacity onPress={()=>{navigation.navigate("PickALake")}}>
        <Text style={styles.bottomText}>
          {'New Friend? Pick a lake and sign up here!'}
        </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};
export default SignIn;
export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.transparent,
    paddingTop: 28,
    paddingLeft: 15,
    paddingRight: 15,
  },
  titleText: {
    color: Colors.primaryBlue,
    fontWeight: 'bold',
    fontSize: 34,
    marginTop: 5,
  },
  forgotText: {
    lineHeight: 20,
    fontSize: 16,
    color: Colors.primaryBlue,
  },
  forgotPressable: {
    marginTop: 11,
    alignSelf: 'flex-end',
  },
  friendsButton: {
    marginTop: 32,
    width: '90%',
    alignSelf: 'center',
    backgroundColor: Colors.PrimaryBrown,
  },
  bottomContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  bottomText: {
    marginBottom: 80,
    color: Colors.primaryBlue,
    lineHeight: 25,
    fontSize: 14,
    fontWeight: '600',
    textDecorationLine: 'underline',
    letterSpacing:1
  },
});
