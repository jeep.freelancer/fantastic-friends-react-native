import React, {useEffect, useState} from 'react';
import {
  View,
  Image,
  Text,
  StyleSheet,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
//assets
import Images from '../../../assets/images';
//Utils
import Colors from '../../../utils/colors';
//Components
import Header from '../../../components/Header';
import Input from '../../../components/TextInput';
import Button from '../../../components/Button';
import {Item} from 'native-base';
import LakeBox from '../../../components/lakeBox';

const lakeData = [
  {id: 1, pic: Images.Conesus, name: 'Conesus'},
  {id: 2, pic: Images.Hemlock, name: 'Hemlock'},
  {id: 3, pic: Images.Canadice, name: 'Canadice'},
  {id: 4, pic: Images.Honeoye, name: 'Honeoye'},
  {id: 5, pic: Images.Keuka, name: 'Keuka'},
  {id: 6, pic: Images.Canandaigua, name: 'Canandaigua'},
  {id: 7, pic: Images.Seneca, name: 'Seneca'},
  {id: 8, pic: Images.Cayuga, name: 'Cayuga'},
  {id: 9, pic: Images.Owasco, name: 'Owasco'},
  {id: 10, pic: Images.Otisco, name: 'Otisco'},
  {id: 11, pic: Images.Skaneateles, name: 'Skaneateles'},
];

const PickALake = ({navigation}) => {
  const [id, setId] = useState(0);
  const renderItem = ({item, index}) => {
    return (
      <LakeBox
        clickAction={() => {
          if (item.id == id) {
            setId(0);
          } else {
            setId(item.id);
          }
        }}
        selected={item.id == id ? true : false}
        pic={item.pic}
        text={item.name}
      />
    );
  };
  return (
    <View style={styles.container}>
      <Header
        leftIconChildren={
          <Ionicons name={'chevron-back'} size={22} color={Colors.black} />
        }
        onLeftAction={() => {
          navigation.goBack();
        }}
      />
      <Text style={styles.titleText}>Pick a lake</Text>
      <View style={{flex: 1, paddingTop: 10}}>
        <FlatList
          data={lakeData}
          extraData={item => item.id}
          renderItem={renderItem}
          numColumns={2}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
        />
      </View>
      {id != 0 && (
        <Button
          clickAction={() =>
            navigation.navigate('SignUp', {lake: lakeData[id - 1]})
          }
          backgroundColorStyle={styles.friendsButton}
          textStyle={{color: Colors.black}}
          text={'Next'}
        />
      )}
    </View>
  );
};
export default PickALake;
export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.transparent,
    paddingTop: 28,
    paddingLeft: 15,
    paddingRight: 15,
  },
  titleText: {
    color: Colors.primaryBlue,
    fontWeight: 'bold',
    fontSize: 34,
    marginTop: 5,
  },
  forgotText: {
    lineHeight: 20,
    fontSize: 16,
    color: Colors.primaryBlue,
  },
  forgotPressable: {
    marginTop: 11,
    alignSelf: 'flex-end',
  },
  friendsButton: {
    marginTop: 20,
    width: '90%',
    alignSelf: 'center',
    backgroundColor: Colors.PrimaryBrown,
    marginBottom: 30,
  },
  bottomContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  bottomText: {
    marginBottom: 80,
    color: Colors.primaryBlue,
    lineHeight: 25,
    fontSize: 14,
    fontWeight: '600',
    textDecorationLine: 'underline',
    letterSpacing: 1,
  },
});
