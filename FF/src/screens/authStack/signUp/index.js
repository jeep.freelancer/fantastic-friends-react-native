import React, {useEffect, useState} from 'react';
import {View, Image, Text, StyleSheet, TouchableOpacity} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
//assets
//Utils
import Colors from '../../../utils/colors';
//Components
import Header from '../../../components/Header';
import Input from '../../../components/TextInput';
import Button from '../../../components/Button';
import LakeBox from '../../../components/lakeBox';

const SignUp = ({navigation, route}) => {
  return (
    <View style={styles.container}>
      <Header
        leftIconChildren={
          <Ionicons name={'chevron-back'} size={22} color={Colors.black} />
        }
        onLeftAction={() => {
          navigation.goBack();
        }}
      />
      <Text style={styles.titleText}>{'Tell Us About You'}</Text>
      <Input placeholder={'First Name'} containerStyle={{marginTop: 28}} />
      <Input placeholder={'Last Name'} containerStyle={{marginTop: 8}} />
      <Input placeholder={'Email'} containerStyle={{marginTop: 8}} />
      <Input placeholder={'Password'} containerStyle={{marginTop: 8}} secure={true} />
      <Input placeholder={'Confirm Password'} containerStyle={{marginTop: 8}} secure={true} />
      <View style={styles.lakeDetailView}>
        <LakeBox
          pic={route.params.lake.pic}
          text={route.params.lake.name}
          backgroundColorStyle={{borderWidth: 0, height: 110, padding: 0}}
        />
        <View>
          <Text style={styles.lakeDescText}>{`Elevation : 715 feet
Area : 11,730 acres
Lenght : 20 miles
Maximum Width : 2 miles
Maximum Depth : 187 feet
Thermocline :`}</Text>
        </View>
      </View>
      <Button
        clickAction={() => navigation.navigate('HomeStack')}
        backgroundColorStyle={styles.friendsButton}
        textStyle={{color: Colors.black}}
        text={'Next'}
      />
    </View>
  );
};
export default SignUp;
export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.transparent,
    paddingTop: 28,
    paddingLeft: 15,
    paddingRight: 15,
  },
  titleText: {
    color: Colors.primaryBlue,
    fontWeight: 'bold',
    fontSize: 34,
    marginTop: 5,
  },
  forgotText: {
    lineHeight: 20,
    fontSize: 16,
    color: Colors.primaryBlue,
  },
  forgotPressable: {
    marginTop: 11,
    alignSelf: 'flex-end',
  },
  friendsButton: {
    marginTop: 32,
    width: '90%',
    alignSelf: 'center',
    backgroundColor: Colors.PrimaryBrown,
  },
  bottomContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  bottomText: {
    marginBottom: 80,
    color: Colors.primaryBlue,
    lineHeight: 25,
    fontSize: 14,
    fontWeight: '600',
    textDecorationLine: 'underline',
    letterSpacing: 1,
  },
  lakeDetailView: {
    flexDirection: 'row',
    marginTop: 24,
  },
  lakeDescText: {
    lineHeight: 24,
    fontSize: 14,
    marginTop: 5,
    color: Colors.black,
  },
});
