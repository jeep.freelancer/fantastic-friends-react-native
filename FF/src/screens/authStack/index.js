import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

const Stack = createStackNavigator();

//Screens
import OnBoarding from './onBoarding';
import SignIn from './signIn';
import PickALake from './pickALake';
import SignUp from './signUp';
/** Auth Stack of the app */
export default AuthStack = () => (
    <Stack.Navigator headerMode="none" initialRouteName='OnBoarding'>
        <Stack.Screen name="OnBoarding" component={OnBoarding} />
        <Stack.Screen name="SignIn" component={SignIn} />
        <Stack.Screen name="PickALake" component={PickALake} />
        <Stack.Screen name="SignUp" component={SignUp} />
    </Stack.Navigator>
);