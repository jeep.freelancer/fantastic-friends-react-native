import React, {useEffect} from 'react';
import {View, Image, Text, StyleSheet} from 'react-native';
//Utils
import Colors from '../../../utils/colors';
//Assets
import Images from '../../../assets/images';
//Components
import Button from '../../../components/Button';

const OnBoarding = ({navigation}) => {
  const Navigate = stackName => {
    navigation.navigate(stackName);
  };
  return (
    <View style={styles.container}>
      <Text style={styles.titleText}>{`Welcome to`}</Text>
      <Text style={styles.descText}>
        {`Fantastic Friends of the Finger Lakes Museum. You are joining a community committed to the Finger Lakes region through inspiration, education, and entertainment. Use this app to add friends and activities to inspire others!`}
      </Text>
      <Image style={styles.lakeImage} source={Images.onBoarding} />
      <Button
        clickAction={() => Navigate('SignIn')}
        backgroundColorStyle={styles.friendsButton}
        textStyle={{color: Colors.black}}
        text={'ALREADY A FRIEND'}
      />
      <Button
        clickAction={() => Navigate('PickALake')}
        backgroundColorStyle={styles.newFriendButton}
        textStyle={{color: Colors.white}}
        text={'New FRIEND? PICK A LAKE'}
      />
      <Text style={styles.bottomDesc}>{'SEE A DASHBOARD EXAMPLE'}</Text>
    </View>
  );
};
export default OnBoarding;
export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
    paddingLeft: 20,
    paddingRight: 20,
  },
  titleText: {
    color: Colors.primaryBlue,
    fontWeight: 'bold',
    fontSize: 34,
    marginTop: 60,
  },
  descText: {
    marginTop: 24,
    lineHeight: 20,
    fontSize: 14,
    color: Colors.black,
    letterSpacing: 2,
    marginBottom: 30,
  },
  lakeImage: {
    width: '100%',
    height: 224,
    resizeMode: 'contain',
  },
  friendsButton: {
    marginTop: 47,
    width: '90%',
    alignSelf: 'center',
    backgroundColor: Colors.PrimaryBrown,
  },
  newFriendButton: {
    marginTop: 16,
    width: '90%',
    alignSelf: 'center',
  },
  bottomDesc: {
    fontSize: 14,
    color: Colors.black,
    marginTop: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    letterSpacing: 1,
    lineHeight: 25,
  },
});
