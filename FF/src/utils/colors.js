export default {
  backgroundSplash: '#171826',
  textSplash: '#E5F0F4',
  transparent: 'transparent',
  primaryBlue: '#00A7E1',
  InActive: '#9CB3BF',
  primaryGreen: '#2AA952',
  tabBackground: '#27283E',
  borderColor: '#3E436D',
  bottomSheetColor: '#25313D',
  black: '#000000',
  PrimaryBrown: '#D29D42',
  white: '#FFFFFF',
  primaryGray: "#C4C4C4"
};
